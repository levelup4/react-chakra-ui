import React from "react";
import { Box, Text } from "@chakra-ui/react";

function App() {
  return (
    <Text
      bgGradient="linear(to-l, #7928CA,#FF0080)"
      bgClip="text"
      fontSize="6xl"
      fontWeight="extrabold"
    >
      Welcome to Chakra UI
    </Text>
  );
}

export default App;
